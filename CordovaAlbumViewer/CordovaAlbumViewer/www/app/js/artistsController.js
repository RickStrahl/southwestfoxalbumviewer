(function() {
    'use strict';

    var app = angular
        .module('app')
        .controller('artistsController', artistsController);
    
    artistsController.$inject = ["$scope", "$http","artistService","errorDisplayService"];

    function artistsController($scope, $http, artistService,errorDisplayService) {
        console.log('artists controller');

        var vm = this; // controller as
        vm.artists = [];
        vm.searchText = "";
        vm.baseUrl = "data/";
        vm.error = errorDisplayService.newErrorModel();

        vm.getArtists = function () {
            return artistService.getArtists()
                .success(function(artists) {
                    vm.artists = artists;
                });
        }

        $scope.$root.$on('onsearchkey', function(e, searchText) {
            vm.searchText = searchText;
        });

        vm.getArtists();

        return;
    }
})();
