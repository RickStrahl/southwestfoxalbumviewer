# Album Viewer AngularJS/Client Side Web Connection Sample Application Sample
from Southwest Fox 2015 

### What it is
This is a heavily modified version of the original MusicStore sample that ships with Web Connection. It is a client centric Web application using AngularJS and Bootstrap that uses a FoxPro backend using JSON to serve the data displayed in the front end application.

The application uses Mobile first design and is designed to run on devices from phones all the way up to desktop class displays and be usable and attractive on all of these device sizes.

**[Live Online Sample Application](https://albumviewerswf.west-wind.com/)**

This application demonstrates:

**On the server side**  
* Using Web Connection as a JSON Service
* Using extensionless URLs in Web Connection
* Using Business Objects using wwBusiness
 
**On the client side**  
* Using AngularJS to create JavaScript based client application
* Using a modified BootStrap template to create a responsive Mobile First UI
