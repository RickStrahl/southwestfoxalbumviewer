﻿"use strict";

var gulp = require('gulp');
var connect = require('gulp-connect');      // server
var open = require('gulp-open');            // browser refresh

var config = {    
    baseUrl: 'http://localhost/albumviewer/',
    page: './index.html',
    paths: {        
        html: './**/*.html',
        js: './**/*.js',
        css: './**/*.css',
        dist: './dist'
    }
};

gulp.task('connect', function() {
    connect.server({
        port: config.port,
        base: config.devBaseUrl,
        livereload: true
    });
});

gulp.task("open", ['connect'], function () {
    gulp.src(config.page)
        .pipe(open({ uri: config.baseUrl }));        
});

//gulp.task('html', function() {
//    gulp.src(config.paths.html)
//        .pipe(connect.reload());
//});

gulp.task('reload', function () {
    gulp.src(config.paths.html)
        .pipe(connect.reload());
});

gulp.task('watch', function () {
    //,config.paths.js,config.paths.css]
    gulp.watch(config.paths.html, ['reload']);
});

gulp.task('default', ['open','watch']);