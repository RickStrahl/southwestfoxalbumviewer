﻿var app = angular.module('app', [
// 'ngRoute',
// 'ngSanitize',
// 'ngAnimate'
]);

// Add Routing
// app.config(['$routeProvider',
//      function($routeProvider) {
//           $routerProvider
//             .when("/albums", {
//                 templateUrl: "app/views/albums.html",
//             })
//             .otherwise({
//                 redirectTo: "/albums"
//             });
//      }
// ]);

app.controller('pageController', pageController);

pageController.$inject = ['$scope', '$http', '$timeout'];

function pageController($scope, $http, $timeout) {

    var vm = this;

    // Interface
    vm.message = null; //"Hello World from AngularJs. JavaScript time is: " + new Date();
    vm.activeTodo = {
        title: null,
        description: null,
        completed: false
    };
    vm.todos = [
        {
            title: "SWFox Angular Presentation",
            description: "Try to show up on time this time",
            completed: false
        },
        {
            title: "Do FoxPro presentation",
            description: "Should go good, let's hope I don't fail...",
            completed: false
        },
        {
            title: "Do raffle at the end of day one",
            description: "Should go good, let's hope I don't fail...",
            completed: false
        },
        {
            title: "Arrive at conference",
            description: "Arrive in Phoenix and catch a cab to hotel",
            completed: true
        }
    ];
    vm.buttonClick = buttonClick;
    vm.addTodo = addTodo;
    vm.removeTodo = removeTodo;
    vm.toggleCompleted = toggleCompleted;
    vm.loadTodos = loadTodos;
    vm.show = showMessage;
    vm.postTodo = postTodo;

    // Initialization
    initialize();

    return;


    // Interface function implementations

    function initialize() {
        console.log("page controller started.");
    }

    function buttonClick() {
        vm.message = "Curious little bugger are you? Good! Updated time is: " + new Date();
    }

    function toggleCompleted(todo) {
        todo.completed = !todo.completed;
    }

    function addTodo() {
        var td2 = $.extend({}, vm.activeTodo);
        console.log(td2);
        vm.todos.splice(0, 0, td2);

        // now also post it to the server
        vm.postTodo($.extend({},vm.activeTodo));

        // clear out todo
        vm.activeTodo.title = null;
        vm.activeTodo.description = null;
    }

    function removeTodo(todo, ev) {
        var index = vm.todos.indexOf(todo);
        if (index > -1)
            vm.todos.splice(index, 1);
    }

    function loadTodos() {
        $http.get('../api/todos')
            .then(function (response) {
                // simply assign data to model
                debugger;
                    vm.todos = response.data;                    
                },
                function() {
                    vm.message = "An error occurred.";
                });
    }

    function postTodo(todo) {
        $http.post('../api/todo', todo)
            .then(function(response) {
                vm.show("To do saved on the server...");
            }, function(response) {
                vm.show("Unable to save todo on the server...");
            });

    }

    function showMessage(msg, icon) {        
        vm.message = msg;
        $timeout(function() {
            vm.message = null;
        },5000);
    }
}
