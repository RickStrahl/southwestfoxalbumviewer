*** Generates SCC XML files from project and vcx,scx and mnx files
*** Uses Christof Wollenhaupt's TwoFox tools to two way convert

*** Pass a parameter to generate binary files from the XML 
*** files restoring back to vcx,scx,mnx etc.
LPARAMETERS llToCode

IF !llToCode
    CLOSE ALL
    SET CLASSLIB TO 
	DO GENXML WITH "Albumviewer.pjx"
ELSE
    DO GENCODE WITH "Albumviewer.twofox"
ENDIF


