************************************************************************
* Project Build routine
****************************************
***  Function: Builds EXE, configures DCOM and optionally uploads
***            
***    Assume:
***      Pass:
***    Return:
************************************************************************
LPARAMETER llCopyToServer
LOCAL lcFile, lcTemplate, lcOLEFlag, lcNewFile

DO wwUtils

*** Configure these options to perform tasks automatically
EXE_FILE		  =		  	"Albumviewer"
DCOM_ProgId       =			"Albumviewer.AlbumviewerServer"
DCOM_UserId		  =			"Interactive User"

*** Server Update Urls - fix these to point at your production Server/Virtual
HTTP_UPLOADURL    =         "http://albumviewerswf.west-wind.com/admin/wc.wc?wwmaint~FileUpload"
HTTP_UPDATEURL 	  =         "http://albumviewerswf.west-wind.com/admin/wc.wc?_maintain~UpdateExe"

*** Optional - User name to pre-seed username dialog
WEB_USERNAME 	  =	  		StrExtract(sys(0),"# ","")

*** Make sure classes is in the path
*DO PATH WITH "\wconnect\classes"

SET PROCEDURE TO 
SET CLASSLIB TO

BUILD EXE (EXE_FILE) FROM (EXE_FILE) recompile

*** Released during build
DO wwHttp

IF FILE(EXE_FILE + ".err")
   MODIFY COMMAND (EXE_FILE + ".err")
   RETURN
ENDIF

DO DCOMCnfgServer with DCOM_PROGID, DCOM_USERID

IF llCopyToServer
   oIP = CREATEOBJECT("wwHttp")

   lcUserNameHttp = InputForm(PADR(WEB_USERNAME,15),"Http Username","Code Update")
   IF EMPTY(lcUsernameHttp)
      RETURN
   ENDIF
   
   lcPasswordHttp = GetPassword("Please enter your HTTP password") 
   IF EMPTY(lcPasswordHttp)
      RETURN
   ENDIF  
         
   WAIT WINDOW "Uploading file to server." + CHR(13) + ;
               "This could take a bit..." NOWAIT
   
   loIp = CREATEOBJECT("wwHttp")
   IF !EMPTY(lcUsernameHttp)
       loIP.cUsername = lcUsernameHttp
       loIP.cPassword = lcPasswordHttp
   ENDIF
   loIp.nHttpPostMode = 2
   loIP.AddPostKey("File",FULLPATH(EXE_FILE + ".exe"),.T.)
   lcHtml = loIp.HttpGet(HTTP_UPLOADURL)
   IF (loIP.nError != 0) OR ATC("File has been uploaded",lcHtml) = 0
      MESSAGEBOX("Upload failed." + CHR(13) + ;
                 loIp.cErrorMsg)
      RETURN
   ENDIF

   WAIT WINDOW "File upload completed..." nowait
   
   lcURL = InputForm(PADR(HTTP_UPDATEURL,100),"URL to update Exe","Update")
   
   wait window nowait "Updating Exe File on Web Server..."
   loIp = CREATEOBJECT("wwHttp")
   lcHTML = oIP.HTTPGet(lcUrl,lcUserNameHttp, lcPasswordHttp)
   
   wait clear
   IF !"Exe Updated" $ lcHTML
      ShowHTML(lchTML)
   ELSE
      MESSAGEBOX("Update completed",64,"Web Connection Server Code Update")
   ENDIF
ENDIF