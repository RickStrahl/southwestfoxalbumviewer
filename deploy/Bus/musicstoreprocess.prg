************************************************************************
*PROCEDURE MusicStoreProcess
****************************
***  Function: Processes incoming Web Requests for MusicStoreProcess
***            requests. This function is called from the wwServer 
***            process.
***      Pass: loServer -   wwServer object reference
*************************************************************************
LPARAMETER loServer
LOCAL loProcess
PRIVATE Request, Response, Server, Session, Process
STORE NULL TO Request, Response, Server, Session, Process

#INCLUDE WCONNECT.H

loProcess = CREATEOBJECT("MusicStoreProcess", loServer)
loProcess.lShowRequestData = loServer.lShowRequestData

IF VARTYPE(loProcess)#"O"
   *** All we can do is return...
   RETURN .F.
ENDIF

*** Call the Process Method that handles the request
loProcess.Process()

*** Explicitly force process class to release
loProcess.Dispose()

RETURN

*************************************************************
DEFINE CLASS MusicStoreProcess AS WWC_RESTPROCESS
*************************************************************

cResponseClass = [WWC_PAGERESPONSE]

*********************************************************************
* Function MusicStoreProcess :: OnProcessInit
************************************
*** If you need to hook up generic functionality that occurs on
*** every hit against this process class , implement this method.
*********************************************************************
FUNCTION OnProcessInit

Response.GzipCompression = .T.
Request.lUtf8Encoding = .T.

Response.AppendHeader("Access-Control-Allow-Origin","*")

RETURN .T.
ENDFUNC

*************************************************************
*** PUT YOUR OWN CUSTOM METHODS HERE                      
*** 
*** Any method added to this class becomes accessible
*** as an HTTP endpoint with MethodName.Extension where
*** .Extension is your scriptmap. If your scriptmap is .rs
*** and you have a function called Helloworld your
*** endpoint handler becomes HelloWorld.rs
*************************************************************

************************************************************************
*  Index
****************************************
***  Function: Return an HTML page for the Index page
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION Index()
JsonService.IsRawResponse = .t.
Response.ContentType = "text/html"
Response.ExpandTemplate(Request.GetPhysicalPath())
RETURN
ENDFUNC
* Index

************************************************************************
*  Artists
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION Artists()
LOCAL loBusArtist, loArtists, loEnv

#IF .F. 
LOCAL Request as wwRequest, Response as wwPageResponse
#ENDIF

loBusArtist = CREATEOBJECT("cArtist")

loArtists = null

*loEnv = CREATEOBJECT("wwEnv","EngineBehavior","70")
lnEB = SET("ENGINEBEHAVIOR")
SET ENGINEBEHAVIOR 70
lnCount = loBusArtist.Query("Select distinct Artists.*, "+;
						"(select COUNT(pk) from albums " + ;
						"   where artistPk = artists.pk) " +;
						"   as Count from Artists, Albums " +;
						"WHERE artists.pk == albums.artistPk " +;
						"ORDER BY artists.artistName",;
						"TArtists",64)						
SET ENGINEBEHAVIOR (lnEb)
							
IF (lnCount > 0)
   loArtists = loBusArtist.vResult
ELSE
   ERROR "No artists could be found."
ENDIF

RETURN loArtists
ENDFUNC
*   Artists

************************************************************************
*  Artist
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION Artist(loArtist)

#IF .F. 
LOCAL Request as wwRequest, Response as wwPageResponse
#ENDIF

lcVerb = Request.GetHttpverb()

*** Handle REST Operations
IF lcVerb = "POST" OR lcVerb = "PUT"
   *** Update or create new artist
   RETURN THIS.UpdateArtist(loArtist)  && updated/new artist
ENDIF
IF lcVerb = "DELETE"
   loBusArtist = CREATEOBJECT("cArtist")   
   RETURN loBusArtist.Delete(lnPk)  && .T. or .F.
ENDIF

lnPk = VAL(Request.QueryString("id"))

loBusArtist = CREATEOBJECT("cArtist")

IF (!loBusArtist.Load(lnPk))
   ERROR loBusArtist.cErrorMsg
ENDIF   

loBusArtist.LoadAlbums()

RETURN loBusArtist.oData
ENDFUNC
*   Artist

************************************************************************
*  UpdateArtist
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION UpdateArtist(loArtist)

IF VARTYPE(loArtist) # "O"
	ERROR "Invalid data passed."
ENDIF

lnPk = loArtist.pk

loBusArtist = CREATEOBJECT("cArtist")
IF lnPk = 0
	loBusArtist.New()
ELSE
	IF !loBusArtist.Load(lnPk)
	   ERROR "Invalid Artist Id."
	ENDIF
ENDIF 

loArt = loBusArtist.oData
loArt.Descript = loArtist.Descript
loArt.ArtistName = loArtist.ArtistName
loArt.ImageUrl = loArtist.ImageUrl
loArt.AmazonUrl = loArtist.AmazonUrl

IF !loBusArtist.Validate() OR ! loBusArtist.Save()
    ERROR loBusArtist.cErrorMsg
ENDIF

loBusArtist.LoadAlbums()

RETURN loArt
ENDFUNC
*   UpdateArtist

************************************************************************
*  Albums
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION Albums()
LOCAL loBusAlbum, loAlbums

loBusAlbum = CREATEOBJECT("cAlbum")
loAlbums = null

*** Load albums individually and then load
*** related artist and songs via bus object
*** this way we get a nested JSON structure
IF loBusAlbum.GetAlbumPkList() > -1
   loAlbums = CREATEOBJECT("Collection")
   SCAN
   	   loBusAlbum.Load(TAlbums.Pk)    
   	   loAlbums.Add(loBusAlbum.oData)
   ENDSCAN
   USE IN TAlbums
ENDIF

RETURN loAlbums
ENDFUNC
*   Albums

************************************************************************
*  Album
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION Album(loAlbum)

lcVerb = Request.GetHttpVerb()

*** Handle alternate verbs with same URL with
*** separate method since we don't have method overloading in VFP
IF (lcVerb == "POST" OR lcVerb == "PUT")
	RETURN THIS.SaveAlbum(loAlbum)
ENDIF

IF lcVerb == "DELETE"
   RETURN this.DeleteAlbum()
ENDIF   

lnId = VAL(Request.QueryString("id"))
IF (lnId < 1)
   ERROR "Invalid Id passed"
ENDIF

loBusAlbum = CREATEOBJECT("cAlbum")
IF (!loBusAlbum.Load(lnId))
   ERROR loBusAlbum.cErrorMsg
ENDIF

RETURN loBusAlbum.oData
ENDFUNC
*   Album

************************************************************************
*  SaveAlbum
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION SaveAlbum(loAlbum)
LOCAL lnId, album, loBusAlbum

IF VARTYPE(loAlbum) # "O"
   ERROR "No album provided to save."
ENDIF   

lnId = loAlbum.pk

loBusAlbum = CREATEOBJECT("cAlbum")

IF (lnId < 1 OR !loBusAlbum.Load(lnId))
   *** Create new instance
   loBusAlbum.New()
   lnId = loBusAlbum.oData.Pk
ENDIF

loBusAlbum.oData = loAlbum
loBusAlbum.oData.Pk = lnId

IF !loBusAlbum.Validate()
   ERROR loBusAlbum.cErrorMsg
ENDIF

IF !loBusAlbum.Save()
   ERROR "Unable to save album"
ENDIF

*** Return the album with update data
RETURN loBusAlbum.oData
ENDFUNC
*   SaveAlbum

************************************************************************
*  DeleteAlbum
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION DeleteAlbum()

lnId = VAL(Request.QueryString("id"))

loBusAlbum = CREATEOBJECT("cAlbum")
IF !loBusAlbum.Delete(lnId)
   ERROR loBusAlbum.cErrorMsg
ENDIF

RETURN .T.
ENDFUNC
*   DeleteAlbum


************************************************************************
*  Reindex
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION Reindex()

IF !USED("Albums")
   USE Albums IN 0
ENDIF

SELECT Albums


ENDFUNC
*   Reindex
*** It is recommend you override the following methods:
***
*** ErrorMsg
*** StandardPage
*** OnError

ENDDEFINE